// -----------------------------------------------------------------------------
// Step.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#include "Step.h"
#include "SevenSegment.h"
#include "Timer.h"
#include <iomanip>
#include <iostream>

namespace WMS {

Step::Step()
{
  std::cout << "Step()" << std::endl;
}

Step::Step(Type step_type, uint32_t step_length)
: type {step_type}, duration {step_length}
{
  std::cout << "Step(" << int(type) << ", " << duration << ')' << std::endl;
}

Step::  ~Step()
{
  std::cout << "~Step(" << int(type) << ")" << std::endl;
}

void Step::run()
{
  std::cout << std::fixed << std::setprecision(2);
  std::cout << "Step '" << get_type_name() << "' "
            << "running for " << (duration / 1000.0) << " seconds\n";
  sleep(duration);
}

const char* Step::get_type_name() const
{
  switch (type) {
    case Type::invalid: return "invalid";
    case Type::empty: return "empty";
    case Type::fill: return "fill";
    case Type::heat: return "heat";
    case Type::wash: return "wash";
    case Type::rinse: return "rinse";
    case Type::spin: return "spin";
    case Type::dry: return "dry";
    case Type::complete: return "complete";
  }
  return "UNKNOWN";
};

} // namespace
