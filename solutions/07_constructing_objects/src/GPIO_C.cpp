// -----------------------------------------------------------------------------
// GPIO_C.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#include "GPIO_C.h"

namespace Devices {

void moder_set_as_output(volatile uint32_t* const reg,
                                uint32_t bit)
{
  auto moder = *reg;
  moder &= ~(0b11u << (bit * 2)); // clear mode
  moder |= (0b01u << (bit * 2));  // set as output
  *reg = moder;
}

void moder_set_as_input(volatile uint32_t* const reg,
                               uint32_t bit)
{
  auto moder = *reg;
  moder &= ~(0b11u << (bit * 2)); // ste as input
  *reg = moder;
}

void odr_set(volatile uint32_t* const reg, uint32_t pattern)
{
  auto value = *reg;
  value |= pattern;
  *reg = value;
}

void odr_clear(volatile uint32_t* const reg,
                      uint32_t pattern)
{
  auto value = *reg;
  value &= ~pattern;
  *reg = value;
}

void odr_invert(volatile uint32_t* const reg,
                       uint32_t pattern)
{
  auto value = *reg;
  value ^= pattern;
  *reg = value;
}

uint32_t idr_read(volatile uint32_t const* const reg)
{
  return *reg;
}

} // namespace 

