// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#include "Step.h"
#include <cassert>

using WMS::Step;

int main()
{
  Step empty {Step::Type::empty, 100};
  Step fill {Step::Type::fill, 100};
  Step heat {Step::Type::heat, 500};
  Step wash {Step::Type::wash, 1000};
  Step rinse {Step::Type::rinse, 1500};
  Step spin {Step::Type::spin, 500};
  Step dry {Step::Type::dry, 100};
  Step complete {Step::Type::complete, 1000};

  empty.run();
  fill.run();
  heat.run();
  wash.run();
  rinse.run();
  spin.run();
  dry.run();
  complete.run();

  Step blank {};
  assert(!blank.is_valid());
  assert(wash.is_valid());
}
