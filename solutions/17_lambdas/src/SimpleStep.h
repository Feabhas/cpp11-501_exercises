// -----------------------------------------------------------------------------
// SimpleStep.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#pragma once
#ifndef SIMPLESTEP_H_
#define SIMPLESTEP_H_

#include <cstdint>
#include "Step.h"

namespace WMS {

class SimpleStep : public Step {
public:
  SimpleStep() = default;
  explicit SimpleStep(Type step_type, uint32_t step_length);
  
  void run() override;
};

} // namespace

#endif // SIMPLESTEP_H_
