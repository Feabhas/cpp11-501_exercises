// -----------------------------------------------------------------------------
// WashProgramme.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <iostream>
#include <algorithm>
#include "WashProgramme.h"
#include "SevenSegment.h"

static constexpr unsigned init_size {12};

namespace {
  using WMS::Step;
  const char* get_type_name(const Step& step)
  {
    using Type = Step::Type;
    switch (step.get_type()) {
      case Step::Type::invalid: return "invalid";
      case Type::empty: return "empty";
      case Type::fill: return "fill";
      case Type::heat: return "heat";
      case Type::wash: return "wash";
      case Type::rinse: return "rinse";
      case Type::spin: return "spin";
      case Type::dry: return "dry";
      case Type::complete: return "complete";
    }
    return "UNKNOWN";
  };
}

namespace WMS {

WashProgramme::WashProgramme()
{
  steps.reserve(init_size);
}

WashProgramme::WashProgramme(std::initializer_list<Step*> init_steps)
: steps {init_steps}
{ }

bool WashProgramme::add(Step& step)
{
  steps.push_back(&step);
  return true;
}

void WashProgramme::run()
{
  for (auto step : steps) {
    std::cout << std::fixed << std::setprecision(2);
    std::cout << "Step '" << get_type_name(*step) << "' "
              << "running for " << (step->get_duration() / 1000.0) << " seconds\n";
    if (display) {
      display->display(unsigned(step->get_type()));
    }
    step->run();
  }
}

void connect(WashProgramme& wash, Devices::OutputDevice& output)
{
  wash.display = &output;
}

uint32_t WashProgramme::get_duration() const
{
  unsigned total {};
  std::for_each(std::begin(steps), std::end(steps),
    [&total](auto const& step) { total += step->get_duration(); }
  );
  return total;
}

} // namespace Application
