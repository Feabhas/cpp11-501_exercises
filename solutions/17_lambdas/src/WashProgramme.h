// -----------------------------------------------------------------------------
// WashProgramme.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#pragma once
#ifndef WASHPROGRAMME_H_
#define WASHPROGRAMME_H_

#include <vector>
#include "Step.h"
#include "OutputDevice.h"

namespace Devices {
class OutputDevice;
}

namespace WMS {

class WashProgramme {
public:
  WashProgramme();
  WashProgramme(std::initializer_list<Step*> init_steps);

  bool add(Step& step);
  void run();

  uint32_t get_duration() const;

private:
  friend void connect(WashProgramme& wash, Devices::OutputDevice& output);

  using Container = std::vector<Step*>;
  Container steps {};
  Devices::OutputDevice* display;
};

} // namespace Application

#endif // WASHPROGRAMME_H_
