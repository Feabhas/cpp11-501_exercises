// -----------------------------------------------------------------------------
// Motorised_step.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#pragma once
#ifndef MOTORISED_STEP_H_
#define MOTORISED_STEP_H_

#include "Step.h"

namespace Devices {
  class Motor;
}

namespace WMS {

class MotorisedStep : public Step {
public:
  explicit MotorisedStep(Type step_type, uint32_t step_length,
                          Devices::Motor& motor);
  ~MotorisedStep() override;

  void run() override = 0;

protected:
  void activate_motor(uint32_t on_time, uint32_t off_time);

private:
  Devices::Motor& motor;
};

} // namespace

#endif // MOTORISED_STEP_H_
