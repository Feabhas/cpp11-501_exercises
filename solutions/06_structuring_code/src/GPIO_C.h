// -----------------------------------------------------------------------------
// GPIO_C.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#pragma once
#ifndef GPIO_C_H_
#define GPIO_C_H_

#include <cstdint>

namespace Devices {

constexpr uint32_t GPIOD_base { 0x4002'0C00u };

static volatile uint32_t *const GPIOD_moder = reinterpret_cast<uint32_t*>(GPIOD_base);
static volatile uint32_t *const GPIOD_odr = reinterpret_cast<uint32_t*>(GPIOD_base + 0x14u);
static volatile const uint32_t *const GPIOD_idr = reinterpret_cast<uint32_t*>(GPIOD_base + 0x10u);

void moder_set_as_output(volatile uint32_t* const reg, uint32_t bit);
void moder_set_as_input(volatile uint32_t* const reg, uint32_t bit);
void odr_set(volatile uint32_t* const reg, uint32_t pattern);
void odr_clear(volatile uint32_t* const reg, uint32_t pattern);
void odr_invert(volatile uint32_t* const reg, uint32_t pattern);
uint32_t idr_read(volatile uint32_t const* const reg);

} // namespace

#endif
