// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "GPIO.h"
#include "SevenSegment.h"
#include "Motor.h"
#include "SimpleStep.h"
#include "WashStep.h"
#include "SpinStep.h"
#include "RinseStep.h"
#include "WashProgramme.h"
#include "Console.h"

using WMS::Step;
using WMS::SimpleStep;
using WMS::WashStep;
using WMS::SpinStep;
using WMS::RinseStep;

int main()
{
  Devices::GPIO gpiod {STM32F407::AHB1_Device::GPIO_D};
  Devices::SevenSegment sseg {gpiod};
  Devices::Motor motor {gpiod};

  SimpleStep fill {Step::Type::fill, 1000};
  SimpleStep heat {Step::Type::heat, 2000};
  WashStep wash {Step::Type::wash, 2500, motor};
  RinseStep rinse {Step::Type::rinse, 2000, motor};
  SpinStep spin {Step::Type::spin, 3000, motor};
  SimpleStep dry {Step::Type::dry, 2400};
  SimpleStep complete {Step::Type::complete, 500};
  SimpleStep empty {Step::Type::empty, 500};

  WMS::WashProgramme white_wash {};

  white_wash.add(fill);
  white_wash.add(heat);
  white_wash.add(wash);
  white_wash.add(empty);
  white_wash.add(rinse);
  white_wash.add(spin);
  white_wash.add(dry);
  white_wash.add(complete);

  connect (white_wash, sseg);
  white_wash.run();

  Devices::Console console {};
  connect (white_wash, console);
  white_wash.run();
}
