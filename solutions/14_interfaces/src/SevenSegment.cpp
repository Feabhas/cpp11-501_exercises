// -----------------------------------------------------------------------------
// SevenSegment.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "SevenSegment.h"
#include "GPIO.h"

namespace Devices {

enum class Pins { A = 8, B, C, D };

SevenSegment::SevenSegment(GPIO& gpio)
: port {&gpio}
{
  gpio.set_output(unsigned(Pins::A));
  gpio.set_output(unsigned(Pins::B));
  gpio.set_output(unsigned(Pins::C));
  gpio.set_output(unsigned(Pins::D));
  blank();
}

SevenSegment::~SevenSegment() 
{ 
  blank(); 
}

void SevenSegment::blank() 
{ 
  port->set(0b11'11u << unsigned(Pins::A));
}

void SevenSegment::display(unsigned value)
{
  port->clear(0b11'11u << unsigned(Pins::A)); 
  port->set((value&0xFu) << unsigned(Pins::A));
}


} // namespace
