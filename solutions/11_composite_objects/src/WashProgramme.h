// -----------------------------------------------------------------------------
// WashProgramme.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#pragma once
#ifndef WASHPROGRAMME_H_
#define WASHPROGRAMME_H_

#include "Step.h"
#include <array>

namespace Devices {
class SevenSegment;
}

namespace WMS {

class WashProgramme {
public:
  WashProgramme() = default;
  bool add(Step::Type step_type, uint32_t duration);
  void run();

private:
  friend void connect(WashProgramme& wash, Devices::SevenSegment& sseg);

  constexpr static unsigned wash_size {12};
  using Container = std::array<Step, wash_size>;

  Container steps {};
  Container::iterator next {std::begin(steps)};

  Devices::SevenSegment* display;
};

} // namespace Application

#endif // WASHPROGRAMME_H_
