// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <cstdint>
#include <iostream>

enum class Steps : uint8_t {
  invalid, empty, fill, heat,
  wash, rinse, spin, dry, complete
};

constexpr unsigned colour_wash_size = 10;

// Array of Step objects
Steps colour_wash[colour_wash_size] = {
    Steps::fill,
    Steps::heat,
    Steps::wash,
    Steps::empty,
    Steps::fill,
    Steps::rinse,
    Steps::empty,
    Steps::spin,
    Steps::dry,
    Steps::complete,
};

int main()
{
  for (unsigned i {}; i < colour_wash_size; ++i) {
    // std::cout << static_cast<unsigned>(colour_wash[i]) << ' ';  // using C+14 cast
    std::cout << unsigned(colour_wash[i]) << ' ';                  // using C+17 cast
  }
  std::cout << std::endl;
}
