// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Step.h"
#include <array>
#include <iostream>

using WMS::Step;

constexpr size_t wash_size {12};

void run_wash(std::array<Step, wash_size>& wash)
{
  std::cout << "Accessing Steps via objects\n";
  for (auto& step : wash) {
    if (step.is_valid()) {
      step.run();
    }
  }
}

void run_wash(const std::array<Step*, wash_size>& wash)
{
  std::cout << "Accessing Steps via pointers\n";
  for (auto& step : wash) {
    if (step) {
      step->run();
    }
  }
}

int main()
{
  // Array of Step objects
  //
  std::array<Step, wash_size> colour_wash {
    Step {Step::Type::empty, 100},
    Step {Step::Type::fill, 100},
    Step {Step::Type::heat, 500},
    Step {Step::Type::wash, 1000},
    Step {Step::Type::rinse, 1500},
    Step {Step::Type::spin, 500},
    Step {Step::Type::dry, 100},
    Step {Step::Type::complete, 1000}
  };

  run_wash(colour_wash);

  // Stack based objects
  //
  Step empty {Step::Type::empty, 100};
  Step fill {Step::Type::fill, 100};
  Step heat {Step::Type::heat, 500};
  Step wash {Step::Type::wash, 1000};
  Step rinse {Step::Type::rinse, 1500};
  Step spin {Step::Type::spin, 500};
  Step dry {Step::Type::dry, 100};
  Step complete {Step::Type::complete, 1000};

  // Array of pointers to Step
  // objects partially initialised
  //
  std::array<Step*, wash_size> white_wash {
      &empty,
      &fill,
      &heat,
      &wash,
      &rinse,
      &spin,
      &dry,
      &complete
  };

  run_wash(white_wash);
}
