// -----------------------------------------------------------------------------
// GPIO.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#pragma once
#ifndef GPIO_H_
#define GPIO_H_

#include <cstdint>
#include "Peripherals.h"

namespace Devices {

class GPIO {
public:
  explicit GPIO(STM32F407::AHB1_Device device);
  void set_input(unsigned pin);
  void set_output(unsigned pin);
  uint32_t mode() const;
  uint32_t read() const;
  void set(uint32_t pattern);
  void clear(uint32_t pattern);

private:
  volatile struct Registers* port;
};

} // namespace

#endif // GPIO_H_
