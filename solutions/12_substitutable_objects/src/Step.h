// -----------------------------------------------------------------------------
// Step.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#pragma once
#ifndef STEP_H_
#define STEP_H_

#include <cstdint>

namespace WMS {

class Step {
public:
  enum class Type {
    invalid, empty, fill, heat,
    wash, rinse, spin, dry, complete
  };
  
  Step() = default;
  explicit Step(Type step_type, uint32_t step_length);
  virtual ~Step() = default;
  
  virtual void run();
  
  bool is_valid() const { return type != Type::invalid; }
  Type get_type() const {return type;}
  uint32_t get_duration() const {return duration;}

private:
  Type type {};
  uint32_t duration {};
};

} // namespace

#endif // STEP_H_
