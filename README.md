# Modern C++ for Embedded Systems (C++11/14/17) Exercises

The exercises have been migrated to GitHub and are no longer maintained here.
 
   * https://github.com/feabhas/cpp11-501_exercises
 
